package com.example.myapplication;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {
    public static final String EXTRA_KEY_FULLNAME="fullName";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
//
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button saveInformationBtn =findViewById(R.id.main_saveInformation_btn);
        saveInformationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this,"Inforamtion is Uploading ...!",Toast.LENGTH_LONG).show();
            }
        });
        CheckBox androidCb = findViewById(R.id.main_android_cb);
        androidCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Toast.makeText(MainActivity.this,"Android Item is Select !",Toast.LENGTH_LONG).show();
            }
        });
        CheckBox deepLearningCb = findViewById(R.id.main_deepLearning_cb);
        deepLearningCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Toast.makeText(MainActivity.this,"DeepLearning Item is Select !",Toast.LENGTH_LONG).show();
            }
        });
        CheckBox uiCb = findViewById(R.id.main_uiux_cb);
        uiCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Toast.makeText(MainActivity.this,"UI/UX Item is Select !",Toast.LENGTH_LONG).show();
            }
        });
        RadioGroup radioGroup = findViewById(R.id.main_radioGroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.main_tehran_rb:
                        Toast.makeText(MainActivity.this,"You Select Tehran city !",Toast.LENGTH_LONG).show();
                        break;
                    case R.id.main_alborz_rb:
                        Toast.makeText(MainActivity.this,"You Select Alborz city !",Toast.LENGTH_LONG).show();
                        break;
                    case R.id.main_gilan_rb:
                        Toast.makeText(MainActivity.this,"You Select Gilan city !",Toast.LENGTH_LONG).show();
                        break;
                }
            }
        });
        Button editProfileBtn =findViewById(R.id.main_editProfile_btn);
        editProfileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,EditProfileActivity.class);
                TextView fullNameTv =findViewById(R.id.main_userName_tv);
                intent.putExtra(EXTRA_KEY_FULLNAME,fullNameTv.getText());
                startActivityForResult(intent,2002);
            }
        });

        Button viewWebSite  = findViewById(R.id.main_viewWeb_btn);
        viewWebSite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse("http://torob.com"));
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2002 && resultCode == Activity.RESULT_OK && data != null){
            String fullName = data.getStringExtra(EXTRA_KEY_FULLNAME);
            TextView textView = findViewById(R.id.main_userName_tv);
            textView.setText(fullName);
        }
    }
}