package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class EditProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        String fullName = getIntent().getStringExtra(MainActivity.EXTRA_KEY_FULLNAME);
        Button doneBtn =findViewById(R.id.editProfile_done_btn);
        EditText editText = findViewById(R.id.EditProfile_UserName_tv);
        editText.setText(fullName);
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String fullName =editText.getText().toString();
                Intent intent =new Intent();
                intent.putExtra(MainActivity.EXTRA_KEY_FULLNAME,fullName);
                setResult(RESULT_OK,intent);
                finish();
            }
        });
    }
}